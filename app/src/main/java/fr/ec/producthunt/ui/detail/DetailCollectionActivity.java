package fr.ec.producthunt.ui.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import fr.ec.producthunt.R;
import fr.ec.producthunt.data.model.Collection;
import fr.ec.producthunt.data.model.Post;
import fr.ec.producthunt.ui.home.CollectionsFragments;
import fr.ec.producthunt.ui.home.PostsFragments;

public class DetailCollectionActivity extends AppCompatActivity implements CollectionsFragments.Callback, PostsFragments.Callback{
  public static final String COLLECTION_POSTS = "collection_posts";

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);

    FragmentManager fm = getSupportFragmentManager();
    fm.beginTransaction().add(R.id.content_frame, PostsFragments.getNewInstance(obtainCollectionIdFromIntent())).commit();
  }

  private String obtainCollectionIdFromIntent() {

    Intent intent = getIntent();
    if (intent.getExtras().containsKey(COLLECTION_POSTS)) {
      return String.valueOf(intent.getExtras().getLong(COLLECTION_POSTS));
    } else {
      throw new IllegalStateException("Il faut passer l'id de la collection");
    }
  }
  @Override
  public void onClickCollection(Collection collection) {
    Intent intent = new Intent(this, DetailCollectionActivity.class);
    intent.putExtra(DetailCollectionActivity.COLLECTION_POSTS, collection.getId());

    startActivity(intent);
  }
  @Override
  public void onClickPost(Post post) {

    Intent intent = new Intent(this, DetailActivity.class);
    intent.putExtra(DetailActivity.POST_URL_KEY, post.getPostUrl());

    startActivity(intent);
  }
}
